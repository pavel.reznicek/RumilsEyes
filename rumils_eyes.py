#!/usr/bin/env python3

"""The main module of the Rúmil’s Eyes application.
Reads collected character data and trains the OCR AI."""

import keras
import numpy
from keras.models import Sequential
# from keras.layers import Dense, Dropout, Activation, Flatten, BatchNormalization
from keras.layers import Dense, Dropout, Flatten, BatchNormalization
from keras.optimizers import SGD
import data_reader
#import common
import config

KEY_DATA = "data"
KEY_LABELS = "labels"

def randomize_data_and_labels(data, labels):
    "Randomizes given data and labels in parallel"
    datalen = len(data)
    if datalen == len(labels):
        index_map = list(range(datalen))
        numpy.random.shuffle(index_map)
        result = {}
        result[KEY_DATA] = numpy.array([data[i] for i in index_map])
        result[KEY_LABELS] = numpy.array([labels[i] for i in index_map])
        return result
    # else raise an exception
    raise RuntimeError("The given data and labels don’t agree in length")

def train():
    "The main training procedure"
    # Generate dummy data
    label_count = data_reader.get_label_count()
    print("label count:", label_count)
    data = data_reader.get_all_samples()
    labels = data_reader.get_all_label_indices_per_smp()
    shuffled_data_and_labels = randomize_data_and_labels(data, labels)
    shuffled_data = shuffled_data_and_labels[KEY_DATA]
    shuffled_labels = shuffled_data_and_labels[KEY_LABELS]
    x_train = shuffled_data
    print("data shape:", x_train.shape)
    print("labels shape:", shuffled_labels.shape)
    y_train = keras.utils.to_categorical(shuffled_labels, num_classes=label_count)

    model = Sequential()
    # Dense(64) is a fully-connected layer with 64 hidden units.
    # in the first layer, you must specify the expected input data shape:
    # here, 20-dimensional vectors.
    #model.add(Dense(64, activation='relu', input_dim=20))
    input_shape = config.CHARACTER_SAMPLE_SIZE.Get()
    model.add(Dense(64, activation='relu', input_shape=input_shape))
    model.add(BatchNormalization())
    model.add(Dropout(0.5))
    #model.add(Dense(64, activation='relu'))
    #model.add(Dropout(0.5))
    model.add(Flatten())
    model.add(Dense(label_count, activation='softmax'))

    sgd = SGD(lr=0.001, decay=1e-6, momentum=0.9, nesterov=True)
    model.compile(
        loss='categorical_crossentropy',
        optimizer=sgd,
        metrics=['accuracy']
    )

    model.fit(
        x_train, y_train,
        epochs=2000,
        batch_size=30
    )
    #score = model.evaluate(x_test, y_test, batch_size=128)

if __name__ == "__main__":
    train()
