#!/usr/bin/env python3

"""A module to load character image samples
from a directory tree structure into a NumPy array."""

import os
import numpy
import PIL.Image
import config
import common

APP_DIR = config.APP_DIR
IMAGES_DIR = config.IMAGES_DIR
MODEL_DIR = config.MODEL_DIR
CONFIG_DIR = config.CONFIG_DIR
SELECTED_FONTS_LIST_FILE_PATH = config.SELECTED_FONTS_LIST_FILE_PATH

CHARACTER_SAMPLE_SIZE = config.CHARACTER_SAMPLE_SIZE


def get_labels_as_int_array():
    "Get the sedecimal character labels from the directory structure"
    label_dirs = os.listdir(IMAGES_DIR)
    label_ints = (int(l, base=16) for l in label_dirs)
    return numpy.array(sorted(label_ints))

def get_labels_as_string_list():
    "Get the sedecimal character labels as strings sorted by integer value"
    labels_as_int_array = get_labels_as_int_array()
    labels_as_string_list = [common.get_char_label(l) for l in labels_as_int_array]
    return labels_as_string_list

def get_label_count():
    "Get the count of the character labels"
    labels = get_labels_as_int_array()
    label_count = len(labels)
    return label_count

def get_label_path(label):
    "Get the whole path of the given character label directory"
    label_path = os.path.join(IMAGES_DIR, label)
    return label_path

def get_sample_path(label, file_name):
    "Get the whole path of the given sample of the given character label"
    label_path = get_label_path(label)
    sample_path = os.path.join(label_path, file_name)
    return sample_path

def get_sample_file_names_of_label(label):
    """get_sample_file_names_of_label(label: str)
    Get the file names of the samples of the given character label"""
    label_path = get_label_path(label)
    file_names = os.listdir(label_path)
    sorted_file_names = sorted(file_names)
    return sorted_file_names

def get_sample_data(label, file_name):
    "Get NumPy array data from the given image file"
    sample_path = get_sample_path(label, file_name)
    img = PIL.Image.open(sample_path)
    # Convert the image into the greyscale profile
    converted_img = img.convert("L")
    data = numpy.array(converted_img)
    return data

def get_samples_of_label(label):
    """get_samples_of_label(label: str)
    Get the data of the samples of the given character label"""
    file_names = get_sample_file_names_of_label(label)
    lst = []
    for fname in file_names:
        sample_data = get_sample_data(label, fname)
        lst.append(sample_data)
    return numpy.array(lst)

def get_all_samples():
    "Get the data of all the character samples"
    labels = get_labels_as_string_list()
    lst = []
    for label in labels:
        samples_of_label = get_samples_of_label(label)
        lst.append(samples_of_label)
    return numpy.concatenate(lst)

def get_all_labels_per_smp():
    "Get the labels for all the character samples"
    labels = get_labels_as_int_array()
    all_labels_per_sample = []
    for label in labels:
        label_dir = common.get_char_label(label)
        sample_file_names = get_sample_file_names_of_label(label_dir)
        sample_count = len(sample_file_names)
        sample_labels = [label] * sample_count
        all_labels_per_sample += sample_labels
    return numpy.array(all_labels_per_sample)

def get_all_label_indices_per_smp():
    "Get the label indices for all the character samples"
    labels = get_labels_as_int_array()
    all_label_indices_per_sample = []
    for l, label in enumerate(labels):
        label_dir = common.get_char_label(label)
        sample_file_names = get_sample_file_names_of_label(label_dir)
        sample_count = len(sample_file_names)
        sample_label_indices = [l] * sample_count
        all_label_indices_per_sample += sample_label_indices
    return numpy.array(all_label_indices_per_sample)

if __name__ == '__main__':
    LABELS = get_labels_as_string_list()
    LABEL = LABELS[32]
    SAMPLE_FILE_NAMES = get_sample_file_names_of_label(LABEL)
    #SAMPLE_FILE_NAME = SAMPLE_FILE_NAMES[0]
    #SAMPLE_DATA = get_sample_data(LABEL, SAMPLE_FILE_NAME)
    #LABEL_DATA = get_samples_of_label(LABEL)
    #SAMPLE_DATA = get_all_samples()
    LABELS_PER_SAMPLE = get_all_label_indices_per_smp()
    print(LABELS_PER_SAMPLE)
