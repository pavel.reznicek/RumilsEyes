#!/usr/bin/env python3
"""An attempt to gather selected characters
from fonts installed on the system."""

import os
import shutil
#import tempfile
import wx
from matplotlib import font_manager
import config
import common
from cigydd.irange import irange

DEBUG = False

APP_DIR = config.APP_DIR
IMAGES_DIR = config.IMAGES_DIR
MODEL_DIR = config.MODEL_DIR
CONFIG_DIR = config.CONFIG_DIR
SELECTED_FONTS_LIST_FILE_PATH = config.SELECTED_FONTS_LIST_FILE_PATH

CHARACTER_SAMPLE_SIZE = config.CHARACTER_SAMPLE_SIZE

def get_font_file_names():
    "Returns file names of all the fonts installed on the system."
    fnames = sorted(font_manager.findSystemFonts())
    #fnames = list(sorted(set(font_manager.get_fontconfig_fonts())))
    #if DEBUG:
    #    return fnames[0:30]
    #else:
    return fnames

def get_font_names():
    "Returns family names of all the fonts installed on the system."
    file_names = get_font_file_names()
    font_names = []
    for file_name in file_names:
        try:
            font_props = font_manager.FontProperties(fname=file_name)
            font_name = font_props.get_name()
            font_names.append(font_name)
        except RuntimeError as err:
            if DEBUG:
                print("""Error while getting font name for file “%s”.
Error class: %s
Message: “%s”."""%(file_name, err.__class__.__name__, err))

    family_names = list(sorted(set(font_names)))
    return family_names

def get_selected_character_codes():
    "Returns some selected character codes to learn."
    char_code_range_defs = [
        (0x00020, 0x0007e), # Basic Latin
        (0x000a1, 0x000ff), # Latin-1 Supplement
        (0x00100, 0x0017f), # Latin Extended A
        (0x00180, 0x0020f), # Latin Extended B
        (0x00250, 0x002af), # IPA Extension
        #(0x002b0, 0x002ff), # Space modifiers
        #(0x00300, 0x0036f), # Combining Diacritical Marks
        #(0x01ab0, 0x01abe), # Combining Diacritical Marks Extension
        #(0x01d00, 0x01d7f), # Phonetic Extensions
        #(0x01d80, 0x01dbf), # Phonetic Extensions Supplement
        #(0x01dc0, 0x01dff), # Combining Diacritical Marks Supplement
        (0x01e00, 0x01eff), # Latin Extended Addition
        (0x02010, 0x02027), # General Punctuation
        #(0x02030, 0x0205e), # General Punctuation
        #(0x02070, 0x02071), # Supersripts and Subscripts
        #(0x02074, 0x0208e), # Supersripts and Subscripts
        #(0x02090, 0x0209c), # Supersripts and Subscripts
        #(0x020a0, 0x020bf), # Currency Symbols
        #(0x02190, 0x021ff), # Arrows
        #(0x02200, 0x022ff), # Math Operators
        #(0x02600, 0x026ff), # Miscellaneous Symbols
        #(0x02700, 0x027bf), # Symbols
        #(0x02c60, 0x02c7f), # Latin Extended C
        #(0x0a720, 0x0a7ae), # Latin Extended D
        #(0x0a7b0, 0x0a7b7), # Latin Extended D
        #(0x0a7f7, 0x0a7ff), # Latin Extended D
        #(0x0ab30, 0x0ab65)  # Latin Extended E
        #(0x1f600, 0x1f64f)  # Emoticons/Emoji
    ]

    character_codes = []
    for range_def in char_code_range_defs:
        start, end = range_def
        character_codes += irange(start, end)

    #character_codes.remove(0x0034f)

    return character_codes

def get_sample_text():
    "Gets the string of selected characters divided into several lines"
    # Get the pre-selected character codes
    character_codes = get_selected_character_codes()
    # Make a string of all selected characters
    result = "".join((chr(code) for code in character_codes))
    return result

def get_text_size(text, font):
    """Creates a bitmap and a wx.MemoryDC and measures the given text in the given font."""
    #bmp = wx.Bitmap(100, 100, depth=8)
    bmp = wx.Bitmap(100, 100)
    if bmp.IsOk():
        try:
            mdc = wx.MemoryDC()
            mdc.SelectObject(bmp)
            extent = mdc.GetFullTextExtent(text, font)
            result = wx.Size(extent[0], extent[1])
            # Correct zero width
            if result.Width <= 0:
                result.Width = 1
            if result.height <= 0:
                result.Height = 1
            return result
        except RuntimeError:
            print(
                "Error while selecting bitmap into a wx.MemoryDC.",
                "Character:", text, "Code:", ord(text)
            )
    raise AssertionError("The bitmap just created is not OK!")

class SelectedFontsList(list):
    """List of selected fonts.
Can be loaded from or saved to a text file."""

    def LoadFromFile(self, fname):
        "Load the list from a text file"
        text_file = open(fname, 'r')
        self.clear()
        for line in text_file:
            item = line.strip()
            if item:
                self.append(item)

    def SaveToFile(self, fname):
        "Save the list to a text file"
        text_file = open(fname, "w")
        text = os.linesep.join(self)
        text_file.write(text)
        text_file.close()

class CharacterDC(wx.MemoryDC):
    "A device context with a character drawn on it"
    #def __init__(self, char=" ", font=wx.Font(), depth=wx.BITMAP_SCREEN_DEPTH):
    def __init__(self, char=" ", font=wx.Font()):
        wx.MemoryDC.__init__(self)
        char_size = get_text_size(char, font)
        width, height = char_size

        # Increase the size to positive numbers if zero
        if width == 0:
            width = 10
        if height == 0:
            height = 10

        #bmp = self.BMP = wx.Bitmap(width, height, depth=depth)
        bmp = self.BMP = wx.Bitmap(width, height)

        # get the drawing context
        self.SelectObject(bmp)

        # fill the canvas with white colour
        self.SetBackground(wx.WHITE_BRUSH)
        self.Clear()

        # set the colours
        self.Pen = wx.BLACK_PEN
        self.Brush = wx.WHITE_BRUSH
        self.SetTextBackground(wx.WHITE)
        self.SetTextForeground(wx.BLACK)

        # draw the character
        self.Font = font
        self.DrawText(char, 0, 0)

    def GetWidth(self):
        """The Width property getter."""
        if self.BMP:
            return self.BMP.Width
        return None
    Width = property(GetWidth)

    def GetHeight(self):
        """The Height property getter."""
        if self.BMP:
            return self.BMP.Height
        return None
    Height = property(GetHeight)

class NormalizedCharacterBitmap(wx.Bitmap):
    """A bitmap containing a character stretched to a standard size."""
    def __init__(self, char=" ", font=wx.Font(), depth=wx.BITMAP_SCREEN_DEPTH):
        wx.Bitmap.__init__(self, CHARACTER_SAMPLE_SIZE, depth)

        src = CharacterDC(char, font, depth)

        # get the drawing context
        dcontext = wx.MemoryDC()
        dcontext.SelectObject(self)

        # fill the canvas with white colour
        dcontext.SetBackground(wx.WHITE_BRUSH)
        dcontext.Clear()

        # set the colours
        dcontext.Pen = wx.BLACK_PEN
        dcontext.Brush = wx.WHITE_BRUSH

        # calculate the destination size
        width, height = self.GetSize()
        src_width, src_height = src.Size
        aspect = float(src_width) / float(src_height)
        # try to set the destination height to the destination bitmap height
        dst_height = height
        dst_width = round(dst_height * aspect)
        # for wider characters
        if dst_width > width:
            dst_width = width
            dst_height = round(dst_width / aspect)
        # avoid zero dimensions
        if dst_width < 1:
            dst_width = 1
        if dst_height < 1:
            dst_height = 1

        if DEBUG:
            print(
                "src_width: {}, src_height: {}, "
                "dst_width: {}, dst_height: {}".format(
                    src_width, src_height,
                    dst_width, dst_height
                )
            )

        # strech draw the character
        dcontext.StretchBlit(
            0, 0, dst_width, dst_height, src, 0, 0, src_width, src_height
        )


class CharacterGathererFrame(wx.Frame): #pylint: disable=too-many-ancestors
    "The character gatherer dialog class"
    def __init__(self, *args, **kw):
        super(CharacterGathererFrame, self).__init__(
            title="Rúmil’s Eyes Character Gatherer", *args, **kw
        )
        szr = wx.BoxSizer(wx.VERTICAL)

        splitter = wx.SplitterWindow(self)

        lst = self.lst = wx.CheckListBox(splitter)
        lst.Bind(wx.EVT_LISTBOX, self.OnLstSelect)

        smp = self.smp = wx.TextCtrl(splitter, style=wx.TE_MULTILINE|wx.TE_READONLY)

        # split the window
        splitter.SplitVertically(lst, smp)
        splitter.SetMinimumPaneSize(20)

        szr.Add(splitter, 1, wx.GROW)

        szrGather = wx.BoxSizer(wx.HORIZONTAL)
        szr.Add(szrGather, 0, wx.GROW)

        btnGather = self.btnGather = wx.Button(
            self, id=wx.ID_OK, label="&Gather characters from selected fonts"
        )
        btnGather.SetDefault()
        szrGather.Add(btnGather, 0, wx.GROW)
        btnGather.Bind(wx.EVT_BUTTON, self.OnBtnGatherClick)

        gaGather = self.gaGather = wx.Gauge(self, style=wx.GA_HORIZONTAL|wx.GA_SMOOTH)
        szrGather.Add(gaGather, 1, wx.GROW)

        btnClose = self.btnClose = wx.Button(
            self, id=wx.ID_CLOSE
        )
        szrGather.Add(btnClose, 0, wx.GROW)
        btnClose.Bind(wx.EVT_BUTTON, self.OnBtnCloseClick)

        self.SetSizer(szr)

        self.Size = (800, 600)
        splitter_size = splitter.GetSize()
        splitter.SetSashPosition((splitter_size[0]) / 2 - (splitter.SashSize + 3))

        self.Bind(wx.EVT_SHOW, self.OnShow)
        self.Bind(wx.EVT_CLOSE, self.OnClose)


    def OnBtnGatherClick(self, evt):
        "Handles click on btnGather"
        self.GatherCharacters()
        evt.Skip()

    def OnBtnCloseClick(self, evt):
        "Handles click on btnCancel"
        self.Close()
        evt.Skip()

    def OnShow(self, evt):
        "Handles frame show event"
        font_names = get_font_names()
        self.lst.Items = font_names
        self.SelectSavedFonts()
        evt.Skip()

    def OnLstSelect(self, evt):
        "Handles font selection in the list"
        font_name = self.lst.StringSelection
        self.smp.Value = get_sample_text()
        info = wx.FontInfo(10).FaceName(font_name)
        font = wx.Font(info)
        self.smp.Font = font
        evt.Skip()

    def OnClose(self, evt):
        "Handles form closing"
        self.SaveSelectedFonts()
        evt.Skip()

    def SelectSavedFonts(self):
        "Selects fonts found in a saved list"
        # If the list file exists,
        if os.path.exists(SELECTED_FONTS_LIST_FILE_PATH):
            # select only the fonts found in the list;
            if DEBUG:
                print("Font list file exists.")
            sfl = SelectedFontsList([])
            sfl.LoadFromFile(SELECTED_FONTS_LIST_FILE_PATH)
            strings = self.lst.Strings
            for i, fname in enumerate(strings):
                if fname in sfl:
                    self.lst.Check(i, True)
                else:
                    self.lst.Check(i, False)
        else:
            # select all fonts.
            if DEBUG:
                print("Font list file doesn’t exist.")
            self.lst.CheckedStrings = self.lst.Strings

    def SaveSelectedFonts(self):
        "Saves the checked fonts list to a file"
        font_names = self.lst.CheckedStrings
        sfl = SelectedFontsList(font_names)
        os.makedirs(CONFIG_DIR, mode=0o755, exist_ok=True)
        sfl.SaveToFile(SELECTED_FONTS_LIST_FILE_PATH)

    def GatherCharacters(self):
        "Actually gathers the selected characters from the fonts"

        # Save the font list
        print("Saving the selected fonts list…")
        self.SaveSelectedFonts()

        # Clean up the data directory
        print("Cleaning up the images directory…")
        wx.SafeYield()
        shutil.rmtree(IMAGES_DIR, ignore_errors=True, onerror=None)
        print("Cleanup done.")
        wx.SafeYield()

        font_names = self.lst.CheckedStrings
        font_counter = 0
        self.gaGather.Value = 0
        self.gaGather.Range = len(font_names)

        for font_name in font_names:

            info = wx.FontInfo(40).FaceName(font_name)
            font = wx.Font(info)
            font_counter += 1 # next font number

            print("Gathering characters from font “%s”…"%font_name)
            wx.SafeYield()

            char_file_name = "%05d.png"%font_counter

            character_codes = get_selected_character_codes()

            for char_code in character_codes:

                char = chr(char_code)

                bmp = NormalizedCharacterBitmap(char, font)

                # ensure the character label directory exists
                char_dir_name = common.get_char_label(char_code)
                char_dir_path = os.path.join(IMAGES_DIR, char_dir_name)
                os.makedirs(char_dir_path, mode=0o755, exist_ok=True)

                # save the image
                char_file_path = os.path.join(char_dir_path, char_file_name)
                #dcontext.SelectObject(wx.NullBitmap)
                img = bmp.ConvertToImage()
                img.SaveFile(char_file_path, wx.BITMAP_TYPE_PNG)
                if DEBUG:
                    print("Saved image %s."%char_file_path)

            self.gaGather.Value += 1
            wx.YieldIfNeeded()

        print("Character gathering done!")


class CharacterGathererApp(wx.App):
    "The character gatherer application class"
    def OnInit(self):
        "Called upon application initialisation"
        result = super().OnInit()
        #wx.InitAllImageHandlers()
        wx.Dialog.EnableLayoutAdaptation(True)
        dlg = CharacterGathererFrame(None)
        self.SetTopWindow(dlg)
        result = result and dlg.Show()
        return result

if __name__ == '__main__':
    APP = CharacterGathererApp(redirect=True)
    APP.MainLoop()
