#!/usr/bin/env python3
"""The configuration bits for Rúmil’s Eyes"""

import os
import appdirs
import wx

APPDIRS_OBJECT = appdirs.AppDirs('Rúmil’s Eyes', 'Cigydd')
APP_DIR = APPDIRS_OBJECT.user_data_dir
IMAGES_DIR = os.path.join(APP_DIR, 'images')
MODEL_DIR = os.path.join(APP_DIR, 'model')
CONFIG_DIR = APPDIRS_OBJECT.user_config_dir
SELECTED_FONTS_LIST_FILE_PATH = os.path.join(CONFIG_DIR, "selected fonts.cfg")

CHARACTER_SAMPLE_SIZE = wx.Size(30, 30)
