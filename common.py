#!/usr/bin/env python3

"""Common code used in more Rúmil’s Eyes modules"""

def get_char_label(char_code):
    """Returns the sedecimal label for given character code
    (used also as a directory name)"""
    sed_char_code = hex(char_code)[2:]
    label = sed_char_code.rjust(5, '0')
    return label
