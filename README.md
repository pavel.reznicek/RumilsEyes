# Rúmil’s Eyes

*An OCR application aimed at custom training of the internal artificial neural 
network written in Python3*

## The Name of the Game
Rúmil is an Elf in the works of J. R. R. Tolkien who invented an alphabet
called Sarati. That was the first known writing system of the Elves. His eyes
must have been extremely sensible in recognizing and reading various symbols, 
and that is where the name of this application comes from: it also recognizes
written characters.

## What’s an OCR System

**OCR** stands for **O**ptical **C**haracter **R**ecognition. Well, the precise
meaning is that with an OCR system, you can transcribe a digital image 
(a scanned page, e. g.) with text on it into a digital text that you can further
edit with a text editor or processor.

## Requirements

- *Python 3*
- the *keras* library for Python 3
    - the *TensorFlow* or *Theano* library for Python 3
    - a computational GPU recommended but not required; otherwise the CPU is 
      used
- the *wxPython* library for Python 3

## The Gathering of Characters

To start up teaching your computer to read, you need to gather some character
samples from the fonts installed on your system. The `character_gatherer.py`
script serves for that purpose. You start it by typing these commands into a 
terminal:
```bash
cd /path/to/rumils_eyes
python3 character_gatherer.py
```
A simple graphical wizard-like window will open where you can select the fonts
you want to gather the character samples from, and then actually gather them.
They will be saved in your application data folder (its path depends on your 
OS).

## Training the Artificial Neural Net

At the time of writing this, the main script, rumils_eyes.py, is a command line
application that takes the gathered character samples and trains an artificial
neural network on them.

Train it by running this command in the terminal (presuming that you already are 
in the Rúmil’s Eyes directory):
```bash
python3 rumils_eyes.py
```

## Things to Come

In the future (writing this on 29th November 2018), this script should be 
transformed into a GUI application able to load an image (a scanned page, e. g.)
and actually identify the characters on it visually and then attempt to 
transcribe them into text.

The **key feature,** however, is planned to be **repeatable custom re-training**
of the network, made possible by correcting errors and adding the corrections 
to the training set.

